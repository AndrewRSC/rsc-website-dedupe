module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
    
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.loadNpmTasks('grunt-combine-media-queries');
    grunt.loadNpmTasks('grunt-combine-mq');
    
    grunt.initConfig({
        sass: {
            options: {
                sourceMap: false,
                outputStyle: 'expanded',
                sourceComments: true,
                precision: 4
            },
            dist: {
                files: {
                    'css/styles.css': 'sass/styles.scss',
                    'css/ie.css': 'sass/ie.scss',
                    'css/shop.css': 'sass/shop.scss',
                    'css/shop-ie.css': 'sass/shop-ie.scss'
                }
            }
        }, // sass
        combine_mq: {
            default_options: {
                expand: true,
            },
            styles_target: {
                options: {
                    beautify: true
                },
                src: 'css/styles.css',
                dest: 'css/styles.css'
                // Target-specific file lists and/or options go here.
            },
            shop_target: {
                options: {
                    beautify: true
                },
                src: 'css/shop.css',
                dest: 'css/shop.css'
                // Target-specific file lists and/or options go here.
            }
        }, // combine media queries
        uglify: {
            script_expanded: {
                options: {
                    beautify: true,
                    mangle: false
                }, // options
                files: {
                    'js/script.js': ['js/comps/*.js']
                } // files
            }, // script.js expanded
            script_mind: {
                options: {
                    compress: {
                        drop_console: true
                    }
                }, // options
                files: {
                    'js/script.min.js': ['js/comps/*.js']
                } // files
            }, // script.js min'd
            shop_mind: {
                options: {
                    compress: {
                        drop_console: true
                    }
                }, // options
                files: {
                    'js/syos.min.js': ['js/syos.js']
                } // files
            } // shop min'd
        }, // uglify
        watch: {
            options: { 
                livereload: true
            }, // options
            scripts: {
                files: ['js/comps/*.js', 'js/syos.js'],
                tasks: ['uglify']
            }, // script
            css: {
                files: ['sass/*.scss'],
                tasks: ['sass', 'combine_mq']
            },
            html: {
                files: ['*.html']
            }
        } // watch
    }); // initConfig
    
    grunt.registerTask('default', 'watch');
} // exports